# disk deco

an experiment to code common categories of designs on recordable CD-Rs as binary bits;
the purpose is to give an approximate guide for visually displaying disks in KaijuTrack or games that use it

this will not be implemented for a while as more information is still needed


```
00 - field holds string instead  [leave the rest of this byte blank]
01 - pattern present or plain
02 - dyed    [whole top of disc has colour if not expected]
03 - tinted  [whole bottom of disc has colour]


06 - mini cd present  [small radius/capacity CD, or irregular shapes]
07 - vinyl present    [disc has record grooves or a vinyl record pattern]


10 - logo present
11 - photo present


?? - digital present
?? - screen tone present
?? - checker present

?? - floral present
```

examples: [see attached links]


---

=> https://www.retrostylemedia.co.uk/product/blank-12cm-green-vinyl-cd-r-700mb-bulk-wrapped  n. patternless dyed vinyl
;
<= blkid  n. KaijuTrack metadata research  ; 1688614762
:: cr. 1696895936
:: t.  deco
