# KaijuTrack metadata research


## blkid

the blkid appears to be the date the filesystem was created given in UTC

2023-06-18-03-46-40-19 -> 2023-06-18T03:46:40.19Z   [*cs]

using my example burned CD, the blkid is stored on the disk as a string at about 0x0000823C, a full three times for some reason:
```
 2023061803464019␀20
23061803464019␀00000
00000000000␀20230618
03464019␀①␀
```


## hub code

discs often have a physical serial number printed on them, such as:
```
N1220C122D810888A1
```

## ATIP codes

## usage & capacity


## volume descriptor for v-disk / KaijuTrack use

the structure of KaijuTrack metadata in the application area (BP 883) will be strongly based on the layout of the volume descriptor itself.

```
000 [1]    - descriptor type - 2
001 [5]    - standard - CD001
006 [1]    - version - 1
040 [31]   - volume name
156 [33]   - root directory entry - maybe specially arranged so deduplicating folders is easy
190 [127]  - volume set

574 [127]  - application identifier - v-disk likely
702 [36]   - copyright/license file
776 [36]   - bibliographic file
813 [17]   - disk creation/"birth" date   - for current revision of disk
830 [17]   - root directory modified date - date for files, not for disk project
847 [17]   - estimated death date  [will not be implemented for quite some time]
864 [17]   - inception date               - creation date for disk revision 1

883 [512]  - application area
  ? [32]   - hub code
  ? [4]    - maximum number of copies of a virtual volume folder (v-disk)
  ? [4]    - disk revision number
  ? [1]    - disk hue (HSL-style integer)
  ?        - disk brand
  ?        - disk expected to be marked on or treated suspiciously
1395 [653] - reserved for extensions to CD001
```



## reading disks

* on a multisession disk — does every session have a volume descriptor?

* where should v-disk file hashes be stored in directory entry for quickly verifying disks



---

[*cs] the last place is centiseconds or "hundredths".

=> https://cdn.kernel.org/pub/linux/utils/util-linux/v2.29/libblkid-docs/libblkid-Miscellaneous-utils.html#blkid-get-dev-size  n. util-linux / blkid-get-dev-size() ;
;
=> https://www.ecma-international.org/wp-content/uploads/ECMA-119_3rd_edition_december_2017.pdf  *e. ECMA-119 (ISO 9660) volume descriptor & filesystem ;
=> https://wiki.osdev.org/ISO_9660  n. ISO 9660 [overview] ;
=> https://github.com/xbmc/libcdio/blob/master/include/cdio/iso9660.h  *g. xbmc / cdio ;
=> https://en.wikipedia.org/wiki/ISO_9660
;
;; multisession
=> https://en.wikipedia.org/wiki/ISO_13490
=> https://superuser.com/questions/916756/how-to-properly-extract-the-content-of-multisession-cd-dvds
=> https://math.nyu.edu/aml/CDwrite.html  n. recording multisession disks with command line ;
;
;; bibliographic info
=> https://www.loc.gov/marc/bibliographic/  n. MARC records [unrelated to PVD] ;
;
=> https://www.ecma-international.org/wp-content/uploads/ECMA-394_1st_edition_december_2010.pdf
=> https://en.wikipedia.org/wiki/Absolute_Time_in_Pregroove  *atip. Absolute Time in Pregroove ;
=> https://ez647.sk/  information about hub codes
;
<= vdisk-research  n. vxsh-suite CD & archival research  ; 1684738487 research
:: cr. 1688614762
:: ed. 1691290759
;    date created for KaijuTrack / date redone for v-disk
:: t.  blkid
:: t.  session
