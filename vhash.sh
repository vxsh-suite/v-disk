#!/bin/bash
# script to tell if changes have been made to a filesystem,
# such as changes to the root filesystem which could be added to official G-refracta/v-refracta image,
# or changes to a personal backup folder
# BUG: this script is currently rather slow - this may be the fault of rhash but I'm not sure
message_device=
vhash_data="/var/lib/vhash"
need_root="y"
bop_id="n"
# add to pkgbuild:
# sudo mkdir -p /var/lib/vhash
# chmod 755 vhash.sh

# assume final argument is directory
dir=${!#}
# get flags
while getopts ":bcv" OPTION; do
 case ${OPTION} in
  b)  # bop - attempt to include bopwiki entry IDs in directory listings
     bop_id="y" ;;
  c)  # cwd - leave hashes in current working directory
     vhash_data=`pwd`
     need_root="n" ;;
  v)  # verbose - print all info messages
     message_device="1" ;;
  ?) echo "Usage: $(basename $0) [-bcv] [directory / command]"
     exit 1 ;;
 esac
done


echo_if_verbose () {
 if [[ -z "$message_device" ]]; then
   echo "" > /dev/null
  else
   echo $1
 fi
}

list_files () {  # list all files in directory recursively
 entries=`tree -ifQ $1`  # list everything in directory
 # sift out only files, and those which exist
 for entry in ${entries}; do
   filename=`echo -n ${entry} | tr -d \"`
   if [[ -f "${filename}" ]]; then
    # add bop entry ID if requested
    if [[ "$bop_id" == "y" ]]; then
      _bop_id=`bop_id ${filename}`  # requires external command
      if [[ -z "${_bop_id}" ]]; then
        echo -n "—  "  # passing -- actually screws up "sort"
       else
        echo -n "${_bop_id}  "
      fi
    fi
    sha256sum ${filename}
   fi
 done
}

backup_file () {  # move old hash file to different name
 _file=$1
 _modified=$2
 _directory=`dirname $_file`
 pushd $_directory > /dev/null
  _date=`date --utc +%Y-%m-%d-%H%M_%SZ --date "@${_modified}"`  # ex. 2023-04-15-0046_29Z
  _movedto="$_file.${_date}"
  mv "$_file" $_movedto
 popd > /dev/null
 echo $_movedto
}

hasher () {
 if [[ $need_root == "y" ]]; then
   data_dir=`realpath -m "${vhash_data}/.$dir"`
  else
   data_dir=`realpath -m "${vhash_data}"`
 fi
 
 mkdir -p ${data_dir} && pushd ${data_dir} > /dev/null
  # if old hashes found, back up
  if [[ -f ./dir ]]; then
   modified=`stat --format="%Y" "${data_dir}/dir"`  # ex. 1681519667
   defaultformat=`date --date "@${modified}"`
   movedto_dir=`backup_file "${data_dir}/dir" "$modified"`
   movedto_digest=`backup_file "${data_dir}/digest" "$modified"`
   echo_if_verbose "Backed up old hashes from ${defaultformat}"
  fi
  
  # create new hashes
  echo_if_verbose "Creating hashes for $dir"
  list_files $dir | sort -s | tee "./dir" 1>&/dev/null
  echo_if_verbose "Created ${data_dir}/dir"
  sha512sum "./dir" > "./digest"
  echo_if_verbose "Created ${data_dir}/digest"
  
  # compare hashes
  if [[ -n "$movedto_digest" ]]; then
    echo_if_verbose "Comparing hashes:"
    compare_digest=`diff "$movedto_digest" ${data_dir}/digest`
    # if digest hash has changed, compare directory hash
    if [[ -n "$compare_digest" ]]; then
      echo_if_verbose "Directory digest has changed - comparing files"
      diff "$movedto_dir" ${data_dir}/dir
     else
      echo_if_verbose "Directory has not changed"
    fi
    else
     echo_if_verbose "Hashes will be compared on next run"
  fi
  # LATER: remove hash backups if hashes stayed the same, so it's clearer how many times the directory changed
 popd > /dev/null
}

if [[ $need_root == "y" && $EUID -ne 0 ]]; then
  echo "This script should be run as root." 1>&2
  exit 1
 elif [[ $need_root == "y" && "$dir" == "purge" ]]; then
  # remove all previous hashes
  echo_if_verbose "Removing all hashes from /var/lib/vhash"
  rm -rf /var/lib/vhash && \
  mkdir -p /var/lib/vhash && \
  exit 0
 # if running as root and directory provided, hash
 elif [[ -n "$dir" && -e "$dir" ]]; then
  hasher
  exit 0
 else
  # exit emitting error & error code
  echo "No directory specified" 1>&2
  exit 2
fi


# LATER
# * hash: /boot /etc /home /root /lost+found /mnt /usr /opt /srv /var
# * exclude vhash's own directory from var in case you've been checking hashes a lot
# * don't follow symlinks
# * properly support git repositories, listing each branch and/or worktree connected to a git repository as if it were a file with its most recent commit hash as its hash
#   * maybe have another 'paranoia level' where the files in the git repository really are hashed one by one. this should really be a separate listing file only containing version control repositories


# https://linuxhandbook.com/bash-test-command/
# https://www.assertnotmagic.com/2018/06/20/bash-brackets-quick-reference/
# https://man.archlinux.org/man/file-hierarchy.7
# https://www.gnu.org/software/coreutils/manual/coreutils.html#basename-invocation
# https://stackoverflow.com/questions/2990414/echo-that-outputs-to-stderr
# https://www.howtogeek.com/778410/how-to-use-getopts-to-parse-linux-shell-script-options/
# https://unix.stackexchange.com/questions/444829/how-does-work-in-bash-to-get-the-last-command-line-argument
# https://serverfault.com/questions/37829/bash-scripting-require-script-to-be-run-as-root-or-with-sudo
