;;;; dredge.lisp - for retrieving data from successfully burned data CDs or other such medium  {{{
(defpackage :vxsh-suite.disk.dredge
	(:shadowing-import-from :vxsh-suite.poa
		#:leaf  #:leaf-p #:make-leaf
		#:leaf-path #:leaf-hash #:leaf-id #:leaf-stem #:leaf-meta #:leaf-body)
	(:shadowing-import-from :vxsh-suite.hash
		#:shallow-list-directories
		)
	(:use :common-lisp)
	(:local-nicknames
		(:atts  :org.shirakumo.file-attributes)
		(:poa   :vxsh-suite.poa)
		(:vhash :vxsh-suite.hash))
		;; :uiop
	(:export
		;; #::cp-r #::mv
		
		;; #::make-disk #::disk-instance #::disk-title  #::disk-name<
		;; #::disk-fullname #::disk-basename
		
		#:list-disks
		;; #::remove-copies
		#:rip-disk
		
		;; #::read-sha-file
		#:decrypt-disk #:extract-disk
))
(in-package :vxsh-suite.disk.dredge)
;; }}}

;;; unix functions - temporary, will be moved to vxsh-suite coreutils interface {{{

(defun cp-r (source dest  &key no-preserve) ; {{{
	(let (mode)
	(when (equal no-preserve 'mode)
		(setq mode "--no-preserve=mode"))
	
	(uiop:run-program
		(format nil "cp -r ~A ~A ~a"
			source  dest  mode))
)) ; }}}

(defun mv (source dest) ; {{{
	(uiop:run-program
		(format nil "mv ~A ~A"
			source  dest))
) ; }}}

;; }}}

;;; DISK structure {{{
;; LATER: move to new package solely for leaf nonsense

(defun make-disk (&key path instance title)  ; create DISK/LEAF structure for disk folders
	(make-leaf
		:path path  :id instance  :stem title))

(defun disk-instance (disk)  (leaf-id   disk))  ; :instance -> :id
(defun disk-title    (disk)  (leaf-stem disk))  ; :title    -> :stem

(defun disk-name< (x y) ; whether DISK/LEAF should sort before by :title
	(string<
		(disk-instance x)
		(disk-instance y)))

;; }}}

(defun disk-fullname (disk-path)  ; disk folder name as string, ex. SAMPLE.001 {{{
	(let (fullname)
	(when (pathnamep disk-path)
		(setq fullname
			(car (last (pathname-directory disk-path)))
			))
	
	;; remove "." from hidden files
	(when (and (stringp fullname) (equal (elt fullname 0) #\.))
		(setq fullname (subseq fullname 1 (length fullname))))
	
	fullname
)) ; }}}

(defun disk-basename (disk-path)  ; title of disk, ex. SAMPLE.001 -> SAMPLE {{{
	(let* ((fullname (disk-fullname disk-path))
	       (basename fullname)
	       (basename-end (- (length basename) 1))
	       period)
	
	(loop for i  from basename-end  downto 1  until (not (null period))  do
		(when (equal (elt basename i) #\.)  (setq period i)))
	
	(unless (null period)
		(setq basename (subseq basename 0 period)))
	
	basename
)) ; }}}

(defun list-disks ; (source)  list top-level collection of disk folders on CD etc {{{
		(source  &key unique titles text)
"List disk folders within medium SOURCE.
SOURCE must be a path which is already mounted on the filesystem"
;; this package does not currently have the ability to look past the current session into previous sessions, so it is advised to keep extra copies as dotfiles

	(let ((all-disks (shallow-list-directories source :text text))
	      known-disks disks-info)
	
	(dolist (disk all-disks)  ; for every disk folder found
		(let* ((basename (disk-basename disk))
		       (basename-symbol (make-symbol basename)))
		(when  ; if either disk is not in KNOWN-DISKS or not passed :unique
			(or (null unique)  (null (find-if #'(lambda (x) (equalp basename x)) known-disks)))
			(push basename known-disks)  ; collect list of disk titles
			(when (null titles)
				(push                    ; collect list of full disk information
					(make-disk :path disk  :instance (disk-fullname disk)  :title basename)
					disks-info)
					))))
	
	(cond
		((null titles)
			(setq
				;; LATER: I am not totally sure if this correctly picks out the first few copies of a disk
				disks-info (stable-sort disks-info #'disk-name<)
				all-disks  disks-info))
		(t (setq all-disks known-disks)))
	
	all-disks
)) ; }}}

(defun remove-copies (disk-list  &key verbose)  ; helper for rip-disk {{{
;; LATER: verbosity
	(let ((keep (first disk-list))
	      (deleting (reverse (butlast (reverse disk-list))))
	      path  keep-dest)
	
	(dolist (disk deleting)
		(setq path (leaf-path disk))
		(uiop:delete-directory-tree path :validate #'uiop:directory-exists-p)
		(format t "removed directory ~a~%" path))
	
	(setq
		path (leaf-path keep)
		keep-dest
			(merge-pathnames
				(make-pathname :directory (append '(:relative) (list (disk-title keep))))
				(uiop:pathname-parent-directory-pathname path)
				))
	(mv
		(string-right-trim "/" (uiop:unix-namestring path))
		(string-right-trim "/" (uiop:unix-namestring keep-dest)))
	(setf (leaf-path keep) keep-dest)
	(format t "renamed ~a -> ~a" path keep-dest)
	
	keep
)) ; }}}

(defun rip-disk ; (disk-name)  rip specific disk folder from CD etc {{{
		(disk-name  &key source dest (copies 1) verbose)
		;; LATER: there is meant to be a default location disks rip to, just as there is (will be) a default location where directory hashes are stored if you don't keep them in the same directory
		;; LATER: verbosity

	(setq
		source (uiop:ensure-pathname source)
		dest (uiop:ensure-pathname dest))
	(let ((disk-list (list-disks source)) (quantity 0) found-copies first-digest)
	
	;; locate all copies of given DISK
	(loop for disk  in disk-list  until (>= quantity copies)  do
		(when (equalp (disk-title disk) disk-name)
			(push disk found-copies)
			(incf quantity)))
	
	;; attempt to rip disks
	;; LATER: will this require conditions? personally, my disk drive has been very touchy at times and made me restart copy operations
	(format t "Copying ~a to ~a~%" disk-name dest)
	(loop for  disk in found-copies  until (equal first-digest 'equal)  do
		(let* ((from (leaf-path disk)) (instance (disk-instance disk))
			;; make sure copied disk is named as clean fullname
			(true-dest (merge-pathnames
				(make-pathname :directory (append '(:relative) (list instance)))  dest
				)))
		(format t "~a -> ~a~%" from true-dest)
		(cp-r  ; copy folder
			(string-right-trim "/" (uiop:unix-namestring from))
			(string-right-trim "/" (uiop:unix-namestring true-dest))
			:no-preserve 'mode)
		(setf (leaf-path disk) true-dest)  ; update path of disk folder to destination path
		;; try to verify disk
		(multiple-value-bind (dir digest)
			(vhash:directory-hashes true-dest :verbose verbose)
			(setf
				digest (vhash:leaf-hash-string digest)
				(leaf-hash disk) digest)
			(cond
				((equal first-digest 'equal)  '())
				((null first-digest)          (setq first-digest digest))
				((equalp first-digest digest)
					(setq first-digest 'equal)
					(format t "Directory intact, rip complete~%")
					(setq found-copies (list (remove-copies found-copies)))
					)))))
	
	found-copies  ; return list of DISKs copied to DEST
)) ; }}}

;;; unpack {{{

#|
erase-hash:
	rm -f dir.* digest.*
erase-contents:
	rm -rf $(contents)
erase-archive:
	rm -f $(archive)
erase-sums:
	rm -f digest-tar digest-enc digest-*.2
|#

(defun tar-disk (file) ; LATER {{{
	;; contents = contents
	;; tar -cJf $(archive) contents
	;; sha512sum $(archive) > digest-tar
) ; }}}

(defun encrypt-disk (file) ; LATER {{{
	;; (uiop:run-program (format nil "gpg --decrypt ~aMakefile.asc" disk-path) :output :string)
	;; gpg --output $(encrypted) --encrypt --recipient $(encryption) $(archive)
	;; sha512sum $(encrypted) > digest-enc
) ; }}}

(defun read-sha-file (file) ; read a file probably produced by a unix-style SHA program {{{
	;; if passed a string, assume it's the file itself
	;; if passed a pathname, load the file
	(unless (stringp file)
		(setq file (first (uiop:read-file-lines file))))	
	(poa:split-value-at-space file)
) ; }}}

(defun decrypt-disk (disk-path  &key clean) ; unpack .tar.xz from gpg encrypted file {{{
	(setq disk-path (uiop:ensure-pathname disk-path))
	(let* (file-hash saved-hash
		(files (uiop:directory-files disk-path))
		(gpg  ; look for any file ending in .gpg
			(find-if  #'(lambda (x) (equal (pathname-type x) "gpg"))  files))
		(enc  ; look for "digest-enc"
			(merge-pathnames (make-pathname :name "digest-enc")  disk-path))
		(archive
			(merge-pathnames (uiop:parse-unix-namestring "./contents.tar.xz") disk-path))
			)
	;; LATER: verbosity
	
	(handler-case
	(unless (null gpg)
		(when (uiop:file-exists-p enc)
			(setq
				saved-hash (read-sha-file enc)
				file-hash  (read-sha-file (vhash:file-hash-cheat gpg :long-digest t)))
			(unless (equal saved-hash file-hash)
				(error (format nil "Saved hash did not match: ~A ~A~%"
						(vhash:leaf-hash-string saved-hash :abbr t)
						(vhash:leaf-hash-string file-hash  :abbr t))))
			(format t "Hash valid for ~a~%" gpg)
			(unless (null clean)  (uiop:delete-file-if-exists enc)))
		;; if everything is okay, decrypt gpg
		(format t "Decrypting ~a~%" gpg)
		(uiop:run-program
			(format nil "gpg --output ~A --decrypt ~A" archive gpg))
		(unless (null clean)  (uiop:delete-file-if-exists gpg))
		(format t "Archive successfully decrypted~%" gpg))
		;; if passed :clean, erase .gpg & digest
	(error (c) (format t "~A" c)))
	
	archive
)) ; }}}

(defun extract-disk (disk-path) ; LATER {{{
	(let (
		dummy
	)
	
	;; sha512sum $(archive) > digest-tar.2
	;; diff digest-tar digest-tar.2 && tar -xJf $(archive) && echo "Archive successfully extracted"
	;; rm digest-tar digest-tar.2
	
)) ; }}}

(defun pack-disk () ; LATER {{{
	;; pack: | erase-sums erase-hash tar erase-contents
) ; }}}

(defun unpack-disk () ; LATER {{{
	;; unpack: | extract erase-archive  # extract and only then clean
) ; }}}

(defun init-makefile (disk-path) ; create Makefile - LATER {{{
	
) ; }}}

;; }}}

;; decrypt, extract
;; vhash the extracted folder
