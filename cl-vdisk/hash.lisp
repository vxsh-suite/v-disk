;;;; hash.lisp - "vhash" algorithm for summarising directories  {{{
;;   "to tell if changes have been made to a filesystem,
;;   such as changes to the root filesystem which could be added to official G-refracta/v-refracta image,
;;   or changes to a personal backup folder"
;; based on vhash.sh script circa 2023-04; GPL 3 or later
(defpackage :vxsh-suite.hash
	(:use :common-lisp)
	(:local-nicknames
		(:atts :org.shirakumo.file-attributes)
		(:iron :ironclad)
		(:poa :vxsh-suite.poa)
		(:bop-mirror :vxsh-suite.bop.mirror))
	(:export
		#:leaf  #:leaf-p #:make-leaf
		#:leaf-path #:leaf-hash #:leaf-id #:leaf-stem
		
		#:date-format-vhash
		;; #::pathname-directory-string #::pathname-parent-directory-string #::symlink-p #::git-storage-p
		;; #::write-file-string
		#:flat-directory-listing #:shallow-list-directories
		
		#:file-hash-cheat
		#:file-hash #:file-hash-string #:leaf-hash-string
		#:file-sum-line
		;; #::leaf< #::leaf=
		
		;; #::purge-global-hashes #::ensure-leaf-list
		;; #::flatten-directory-hashes
		#:directory-hashes #:load-directory-hashes
		#:pseudo-diff
		
		;; #::archive-hash #::unarchive-hash
		#:vhash
))
(in-package :vxsh-suite.hash)

;; vhash_data="/var/lib/vhash"
;; LATER: this program currently *only* creates hashes for non-system directories within the same directory. it is actually intended to optionally be able to save hashes to a central location such as ~/.local or /var/lib/vhash.
;; }}}


;; LATER: this is a bit slow. just use output streams and remove in a subsequent update
(defun string-push (new existing) ; add line onto string {{{
	(format nil "~a~a~%" existing new)
) ; }}}

#| preferred listing code
	(dir-listing
		(if (null text)  nil  (make-string-output-stream)))
	
	(unless (null text)  ; if text output requested...
		(dolist (file all-files)  ; flatten file list to string by writing to stream
			(format dir-listing "~a~%" file))
		(setq all-files (get-output-stream-string dir-listing)))
	|#

;; internals - filesystem, hashes, poa {{{

;;; filesystem

(defun date-format-vhash (universal &key basename) ; 'traditional' date format for vhash {{{
	(multiple-value-bind
			(sec minute hour day month year) (decode-universal-time universal 0)
		(format nil "~A~A~2,'0d-~2,'0d-~2,'0d-~2,'0d~2,'0d_~2,'0dZ"  ; ex. 2023-04-15-0046_29Z
			(if (null basename)  "" basename)
			(if (null basename)  "" ".")
			year month day hour minute sec))
) ; }}}

(defun pathname-directory-string (directory) ; {{{
;; like uiop:pathname-directory-pathname but for getting the final directory string
	(car (last (pathname-directory directory)))
) ; }}}

(defun pathname-parent-directory-string (directory) ; {{{
;; like uiop:pathname-parent-directory-pathname but for getting the final directory string
	(car (last (pathname-directory (uiop:pathname-parent-directory-pathname directory))))
) ; }}}

(defun symlink-p (path) ; determine if path is "most likely" a symlink {{{
	;; uiop seems bad at comprehending symlinks inside relative paths.
	;; as a workaround, using truenamize parses the resolved and unresolved symlinks with the exact same error of putting them inside `pwd`
	(not (equalp (uiop:resolve-symlinks path) path))
) ; }}}

(defun git-storage-p (git-member) ; identify .git objects folders which should be excluded {{{
	(let (
		(final-dir (pathname-directory-string git-member))
		(parent-dir (pathname-parent-directory-string git-member))
		)
	(and
		(not (null
			(find-if
				#'(lambda (x)
					(and (stringp x) (> (length x) 3) (equal (subseq x (- (length x) 4) (length x)) ".git"))
					)
				(pathname-directory git-member))))
		(or (equal final-dir "objects") (equal final-dir "logs") (equal final-dir "lfs"))
		)
)) ; }}}

(defun flat-directory-listing (directory &key verbose text) ; file paths for "dir" list {{{
"Recursively return all files inside DIRECTORY as a single flat list"
	(let ((true-parent (uiop:resolve-symlinks directory))
		(dir-listing
			(if (null text)  nil  (make-string-output-stream)))
		all-files)
	
	(uiop:chdir true-parent)
	(uiop:collect-sub*directories true-parent
		(constantly t)  ; collect top directory
		(lambda (dir)   ; collect subdirectories that are not .git objects
			(if
				(or  ; exclude folder if it is either...
					(symlink-p dir)       ; ...a symlink
					(git-storage-p dir))  ; ...an internal git folder
				nil  t))
		(lambda (dir)
			(setq all-files
				(append
					(mapcan
						#'(lambda (x)
							(if (symlink-p x)  nil  (list x)))
						(uiop:directory-files dir))
					all-files))))
	
	(unless (null text)  ; if text output requested...
		(dolist (file all-files)  ; flatten file list to string by writing to stream
			(format dir-listing "~a~%" file))
		(setq all-files (get-output-stream-string dir-listing)))
	
	(values all-files true-parent)
)) ; }}}

(defun shallow-list-directories (directory &key text) ; {{{
	(let ((true-parent (uiop:resolve-symlinks directory))
		(dir-listing
			(if (null text)  nil  (make-string-output-stream)))
		all-files)
	
	(uiop:chdir true-parent)
	(uiop:collect-sub*directories true-parent
		(constantly t)  ; collect top directory
		(lambda (dir)   ; collect subdirectories that are not .git objects
			(if
				(or  ; exclude folder if it is either...
					(symlink-p dir)       ; ...a symlink
					(git-storage-p dir))  ; ...an internal git folder
				nil  t))
		(lambda (dir)  ; collect directory names only
			(unless (equal true-parent dir)
				(setq all-files
					(append (list dir) all-files))
				)
			))
	
	(unless (null text)  ; if text output requested...
		(dolist (file all-files)  ; flatten file list to string by writing to stream
			(format dir-listing "~a~%" file))
		(setq all-files (get-output-stream-string dir-listing)))
	
	(values all-files true-parent)
)) ; }}}

(defun write-file-string (string output &key verbose) ; save string to file {{{
	;; note: "verbosity" messages are copied from the original vhash.sh
	(unless (null verbose)  (format t "Created ~a~%" output))
	(with-open-file (s output :direction :output
		:if-exists :rename
		:if-does-not-exist :create)
		(write-sequence string s))
		;; if the file already exists it will be moved to "*.bak"
) ; }}}

(defun archive-hash (filename &key modified destination) ; rename hash with time modified {{{
	(let ((moved-to
		(if (null destination)
			(make-pathname
				:directory (pathname-directory filename)
				:name
					(date-format-vhash modified :basename (pathname-name filename)))
			destination)))
	(uiop:rename-file-overwriting-target filename moved-to)
	moved-to  ; return filename moved to
))

(defun unarchive-hash (filename restore-to) ; "un-rename" hash to old name
	(when (uiop:file-exists-p filename)
		(uiop:delete-file-if-exists restore-to)
		(archive-hash filename :destination restore-to))
) ; }}}

;;; may be moved to bop "poa" recode later

(defstruct leaf  ; file to be summed  - path hash id stem {{{
	path  ; filename
	hash  ; file hash/digest
	id    ; identifier in bop etc
	stem  ; disk or bop stem name
) ; }}}

;;; file sums

(defun file-hash-string (hash-bytes) ; hex string for file hash {{{
	(let* ((hash-end (- (length hash-bytes) 1))  ; last byte of hash
		(hash-string (make-array (* (length hash-bytes) 2)))  ; fixed vector to contain chars
		(hex (vector #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9 #\a #\b #\c #\d #\e #\f)))
	
	(loop for b  from 0 to hash-end  do  ; looping over hash-bytes...
		;; chop the 8-bit bytes into 4-bit bytes
		;; the bit positions are numbered 0-3 and 4-7
		(let* ((hash-byte (aref hash-bytes b))
			(b-lower (ldb (byte 4 0) hash-byte))
			(b-upper (ldb (byte 4 4) hash-byte)))
		;; make each 4-bit byte into a hex digit from 0-f
		(setf  ; set down the half-bytes at...
			(aref hash-string (* b 2))  ; ...every other half-byte from b = 0
				(aref hex b-upper)
			(aref hash-string (- (* (+ b 1) 2) 1))  ; ...twice b from b = 1
				(aref hex b-lower))
	))
	(coerce hash-string 'string)  ; render char vector to string
)) ; }}}

(defun leaf-hash-string (leaf  &key abbr) ; {{{
	(let ((hash leaf))
	(when (leaf-p hash)
		(setq hash (leaf-hash hash)))
	(cond
		((null hash) "")
		((stringp hash)
			(if (or (null abbr) (< (length hash) 7))
				hash
				(format nil "~a ..." (subseq hash 0 7))))
		(t (file-hash-string hash)))
)) ; }}}

#|(defun hash-string-number (hash-string) ; integer for hex string {{{
	(let ((result 0)
		(string-bytes (make-array (length hash-string)))
		(hex (vector #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9 #\a #\b #\c #\d #\e #\f))
	)
	
	
	
)) ; }}}|#

(defun file-hash-cheat (filename  &key long-digest) ; {{{
;; LATER: using the vhash/ironclad digest function on a 120 MiB file took /minutes/ on files that took seconds with GNU programs? currently I have no idea why this is, so I will have to include this "cheat" until I figure it out. setting --dynamic-space-size 1024 did not seem to help
	(if (null long-digest)
		(uiop:run-program (format nil "sha256sum ~a" filename) :output :string)
		(uiop:run-program (format nil "sha512sum ~a" filename) :output :string))
) ; }}}

(defun file-hash (filename &key long-digest text) ; SHA-256/512 digest of file {{{
"Return SHA-256 hash of file at FILENAME.
If :LONG-DIGEST is T, return SHA-512 hash"
	(let ((digest
		(if (null long-digest)
			;; most of the time, hash files using SHA-256 for speed
			(iron:make-digest :sha256)
			;; in a few cases, hash files using SHA-512
			(iron:make-digest :sha512)))
		digest-sequence)
	(with-open-file  ; create byte stream from FILENAME
			(file (uiop:ensure-pathname filename)
			:direction :input :element-type '(unsigned-byte 8))
		(iron:update-digest digest file))  ; add file bytes to hash
	(setq digest-sequence (iron:produce-digest digest))
	(if (null text)  ; if text digest was not requested...
		digest-sequence  ; return byte vector
		(file-hash-string digest-sequence))  ; return hex string
)) ; }}}

(defun file-sum-line (filename &key hash id) ; summary for each file within "dir" list {{{
	(when (leaf-p filename)
		(setq
			hash (leaf-hash filename)
			hash (if (vectorp hash)  (leaf-hash-string hash)  hash)
			filename (leaf-path filename)
			))
	(format nil "~A~A~a  ~a"
		(if (null id)  ""  id)
		(if (null id)  ""  "  ")
		(if (null hash)
			(file-hash filename :text t)  hash)
		filename)
) ; }}}

(defun leaf< (x y) ; BUG: this seems to sort paths backwards? {{{
	(let ((x-num 0) (y-num 0))
	
	;; assume identifiers can only be strings, given that Poa allows serializing entry ID numbers to something other than an integer
	(when (string> (leaf-id x) (leaf-id y))
		(incf x-num 1))
	(when
		(string>
			(leaf-hash-string x)
			(leaf-hash-string y))
		(incf x-num 4))
	(when
		(string>
			(format nil "~a" (leaf-path x))
			(format nil "~a" (leaf-path y)))
		(incf x-num 8))
	
	(< x-num y-num)
)) ; }}}

(defun leaf= (x y) ; whether leaf is "probably" the same file, for pseudo-diff purposes {{{
	(and
		(string=
			(leaf-hash-string x)
			(leaf-hash-string y))
		(string=
			(format nil "~a" (leaf-path x))
			(format nil "~a" (leaf-path y))
			))
) ; }}}

;; }}}

;;; generate "dir" list

(defun purge-global-hashes () ; BUG: unfinished {{{
	;; # remove all previous hashes
	;; echo_if_verbose "Removing all hashes from /var/lib/vhash"
	;; rm -rf /var/lib/vhash && \
	;; mkdir -p /var/lib/vhash && \
	;; exit 0
	
	;; LATER: allow storing hashes within user home directory instead of system directory, and if so, do not require root
	nil
) ; }}}

(defun flatten-directory-hashes (leaf-list stream) ; {{{
	(dolist (file leaf-list)   ; for every LEAF in LEAF-LIST
		(format stream "~a~%"  ; flatten file to string by writing to stream
			(file-sum-line (leaf-path file)
				:hash (leaf-hash-string file)
				:id   (leaf-id file))))
	(get-output-stream-string stream)  ; output stream as string
) ; }}}

(defun directory-hashes (directory &key verbose bop text) ; "dir" and "digest" listings {{{
	(let ((dir-digest (iron:make-digest :sha512))
		(dir-listing
			(if (null text)  nil  (make-string-output-stream)))
		leaf-list)
	
	(let (leaf hash leaf-name leaf-ident)
	(multiple-value-bind (files true-parent)
		(flat-directory-listing directory :verbose verbose)
	(dolist (file files)
		(setq
			leaf-name (pathname-name file)
			hash
				(handler-case
					(file-hash file)
					(error (c)  nil))
				;; I attempted to get out of this with an IF check whether file exists but it refused to work if there was any chance it would check an invalid symlink
			leaf-ident
				(if (null bop)
					nil
					(bop-mirror:entry-identifier file))
			leaf
				(make-leaf :hash hash
					;; make all leaf paths relative to DIRECTORY
					:path (uiop:enough-pathname file true-parent)
					:id
						(if (null bop)
							nil
							(if (null leaf-ident)
								"—"
								(format nil "~a" leaf-ident)))
					))
					;; LATER: implement getting bop entry ID
		
		(unless  ; if this is not a hash file currently produced by vhash, or a broken link
				(or (equal leaf-name "dir") (equal leaf-name "digest")
				(equal (leaf-hash leaf) nil))
			(iron:update-digest dir-digest hash)  ; add to short "digest"
			(push leaf leaf-list))  ; add to long "dir" listing
		
		;; reset loop variables - written this way to avoid two LETs
		(setq leaf nil leaf-name nil hash nil leaf nil))))
	
	(setq
		leaf-list  (stable-sort leaf-list #'leaf<)   ; sort files by hash
		dir-digest (iron:produce-digest dir-digest)) ; retrieve digest bytes
	
	(unless (null text)  ; if text output requested...
		(setq
			leaf-list (flatten-directory-hashes leaf-list dir-listing) ; retrieve full string
			dir-digest (file-hash-string dir-digest)))       ; flatten digest to string
	
	(values leaf-list dir-digest)
)) ; }}}

(defun load-directory-hashes (file &key verbose bop text all-files) ; {{{
	(let ((lines      (if (listp file)  file  (uiop:read-file-lines file)))
	      (dir-listing  (if (null text)   nil   (make-string-output-stream)))
	      leaf exclusion-path path-length exclude dir-hash leaf-list)
	
	;; LATER: attempt calculating "digest" hash if "dir" line not included
	(dolist (line lines)
		(setq  leaf nil  exclusion-path nil  path-length nil  exclude nil)  ; to avoid extra LET
		;; go through conditionals for each kind of LEAF - some things should be done or not done in specific situations, but some things should always be done
		(cond
			((leaf-p line)
				(setq  leaf line  exclusion-path (leaf-path line)))
			(t
				(unless (equal (elt line 0) #\#)  ; treat "#" lines as comments
				;; if this line is not a comment, split it into hash and file path
				(multiple-value-bind
					(hash path) (poa:split-value-at-space line)
				;; get information out of file
				(setq exclusion-path path)
				(if
					(equal path "dir")  (setq dir-hash hash)  ; pick out whole directory sum if present
					;; otherwise add file to LEAF list
					(setq leaf (make-leaf :hash hash :path (uiop:ensure-pathname path)))
					)))))
		
		;; try to determine if leaf should be excluded - at least for now, only check for git objects
		(setq exclusion-path (uiop:ensure-pathname exclusion-path))
		(unless (or (null exclusion-path) (not (null all-files)))
			(setq
				path-length
					(if (null exclusion-path)
						1  (length (pathname-directory exclusion-path))
						))
			(loop  for i  from path-length downto 1
					until (or (not (null exclude)) (null exclusion-path))  do
				(setq
					exclusion-path (uiop:pathname-parent-directory-pathname exclusion-path)
					exclude (git-storage-p exclusion-path)
					)))
		
		(unless (or (null leaf) (not (null exclude)))
			(push leaf leaf-list)
			))
	
	(setq leaf-list  (stable-sort leaf-list #'leaf<))   ; sort files by hash
	
	(unless (null text)  ; if text output requested...
		(setq leaf-list (flatten-directory-hashes leaf-list dir-listing))) ; retrieve full string
		;; currently dir-hash is always a string
	
	(values leaf-list dir-hash)
)) ; }}}

(defun ensure-leaf-list (dir  &key verbose text digest-path) ; unfurl pathname into Lisp "dir" listing {{{
	(let (leaf-list dir-hash)
	
	(cond
		((null dir) '())
		((and (listp dir) (leaf-p (first dir)))
			(multiple-value-bind (listing hash) (load-directory-hashes dir :text text)
				(setq  leaf-list listing  dir-hash hash)))
		;; if DIR is a known filesystem directory, use 'directory-hashes
		((and (pathnamep dir) (uiop:directory-exists-p dir))
			(multiple-value-bind (listing hash) (directory-hashes dir :text text)
				(setq  leaf-list listing  dir-hash hash)))
		;; if DIR is a known text file, use 'load-directory-hashes
		((and (pathnamep dir) (uiop:file-exists-p dir))
			(multiple-value-bind (listing hash) (load-directory-hashes dir :text text)
				(setq  leaf-list listing  dir-hash hash)))
		(t nil))
	
	;; if there is no directory sum but a "digest" file provided, load sum from file
	(when (and (null dir-hash) (uiop:file-exists-p digest-path))
		(setq dir-hash (uiop:read-file-string digest-path)))
	
	(values leaf-list dir-hash)
)) ; }}}

(defun pseudo-diff (dir-a dir-b &key text) ; run a diff-like process on two "dir" listings {{{
	(let ((b-files (make-hash-table :test 'equal))
		(a-files (make-hash-table :test 'equal))
		(removed-files (if (null text)  nil  (make-string-output-stream)))
		(added-files   (if (null text)  nil  (make-string-output-stream)))
		)
	
	(setq
		dir-a (ensure-leaf-list dir-a)
		dir-b (ensure-leaf-list dir-b))
	
	(dolist (file dir-b)
		(setf (gethash (leaf-path file) b-files) file))
	;; check through dir A for files in dir B
	(dolist (file dir-a)
		(setf (gethash (leaf-path file) a-files) file)
		(let ((existing-file
			(gethash (leaf-path file) b-files)))
		(cond
			((leaf= existing-file file)  '())
			((null existing-file)  ; if dir A file not found in dir B
				(if (null text)
					(push file removed-files)
					(format removed-files "< ~a~%" (file-sum-line file))
					))
			((equal (leaf-path existing-file) (leaf-path file))
				(if (null text)
					(push file added-files)
					(format added-files "<> ~a~%" (file-sum-line file))
					))
			(t
				(if (null text)
					(push file added-files)
					(format added-files "> ~a~%" (file-sum-line file))
					)))))
	;; check through dir B for files in dir A
	(dolist (file dir-b)
		(let ((existing-file
			(gethash (leaf-path file) a-files)))
		(cond
			((null existing-file)  ; if dir B file not found in dir A
				(if (null text)
					(push file added-files)
					(format added-files "> ~a~%" (file-sum-line file))
					)))))
	
	(unless (null text)  ; if text output requested...
		(setq  ; flatten file lists to string by writing to stream
			added-files   (get-output-stream-string added-files)
			removed-files (get-output-stream-string removed-files)))
	
	(values added-files removed-files)
)) ; }}}

(defun vhash (directory &key verbose bop cwd text) ; {{{
	(setq directory (uiop:ensure-pathname directory))
	(let* (dir-modified dir-moved digest-moved old-digest digest-is-dir
		(data-dir (uiop:ensure-pathname directory))
		(dir-file (merge-pathnames data-dir (make-pathname :name "dir")))
		(digest-file (merge-pathnames data-dir (make-pathname :name "digest")))
		(make-file (merge-pathnames data-dir (make-pathname :name "Makefile")))
	)
	
	;; LATER:
	;; - create data dir if it doesn't exist
		;; ensure-directories-exist
	;; - copy directory layout inside data dir if not creating in place
		;; if [[ $need_root == "y" ]]; then
		;;  data_dir=`realpath -m "${vhash_data}/.$dir"`
		;; else
		;; data_dir=`realpath -m "${vhash_data}"`
	;; :cwd is intended to specify whether to leave hashes in current directory, but is not yet implemented in this version
	
	;; check Makefile for options
	(let (makefile-data) ; {{{
		(when (uiop:file-exists-p make-file)
			(setq makefile-data (uiop:read-file-lines make-file))
			(dolist (line makefile-data)
				(when (find #\= line)
					(cond
						;; since v-disk 2.6.0, it is possible to keep "digest" inside "dir"
						;; LATER: allow whitespace in statement
						((equal line "digest = dir")
							(setq digest-is-dir t))
						)
					)))) ; }}}
	;; if old summaries found, archive them to different names
	(when (uiop:file-exists-p dir-file) ; {{{
		(unless (null verbose)  (format t "Old hashes found; archiving.~%"))
		(setq
			dir-modified (atts:modification-time dir-file)
			dir-moved (archive-hash dir-file :modified dir-modified))
		;; archive "digest" if it exists
		(cond
			((uiop:file-exists-p digest-file)
				(setq digest-moved (archive-hash digest-file :modified dir-modified)))
			(t  (setq digest-moved nil))
			)) ; }}}
	
	(unless (null verbose)  (format t "Creating hashes for ~a~%" directory))
	;; create new directory summaries
	(multiple-value-bind  ; new hashes
			(dir-listing dir-digest)
			(ensure-leaf-list directory :verbose verbose :digest-path digest-file)
	
	(unless (null verbose)  (format t "Comparing hashes:~%"))
	(multiple-value-bind  ; old hashes
			(old-listing old-digest)
			(ensure-leaf-list dir-moved :verbose verbose :digest-path digest-moved)
		
		;; save "dir" listing & "digest" to file
		(multiple-value-bind  ; stringified hashes
			(string-listing string-digest)
			(ensure-leaf-list directory :verbose verbose :text t)
		(cond
			;; when "digest = dir", put "digest" at the top of "dir"
			((not (null digest-is-dir))
				(write-file-string
					(format nil "~a~%~a"
						(file-sum-line "dir" :hash string-digest)  string-listing)
					dir-file  :verbose verbose))
			(t  ; otherwise, put "dir" listing and "digest" in separate files
				(write-file-string string-listing dir-file    :verbose verbose)
				(write-file-string string-digest  digest-file :verbose verbose)
				))
		(setq dir-digest string-digest))
		
		(unless (null verbose)
			(format t "old digest: ~a~%" (leaf-hash-string old-digest :abbr t))
			(format t "current digest: ~a~%" (leaf-hash-string dir-digest :abbr t)))
		
		(cond
			;; if old version of "digest" contains nothing, stop
			((and (null old-digest) (null dir-moved))
				(unless (null verbose)  (format t "Hashes will be compared on next run~%"))
				)
			;; if short "digest" is the same as old version, stop
			((equal old-digest dir-digest)
				(unless (null verbose)  (format t "Directory has not changed~%"))
				;; remove new digest files and restore old ones
				(unarchive-hash dir-moved dir-file)
				(unarchive-hash digest-moved digest-file)
				)
			(t  ; if short "digest" changed, compare longer "dir" listings
				(unless (null verbose)
					(format t "Directory digest has changed - comparing files~%"))
				(pseudo-diff old-listing dir-listing :text text)
				))
		))
)) ; }}}

(defun approve-changes ()
	;; rm dir.*
)

(defun reject-changes ()
)

(defun disk-changes ()
	;; diff dir.* dir
	;; LATER: prompt to accept/reject?
)
